package com.grape.rabbitmq.queue.core.publishandsubscribe;

import com.grape.rabbitmq.queue.util.MQConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 描述:发布订阅队列生产者
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class PSProducer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.获取连接
        Connection newConnection = MQConnectionUtils.newConnection(PSConstant.VIRTUALHOST_NAME);
        // 2.创建通道
        Channel channel = newConnection.createChannel();
        // 3.绑定的交换机 参数1交互机名称 参数2 exchange类型
        channel.exchangeDeclare(PSConstant.EXCHANGE_NAME, "fanout");
        String msg = "fanout_exchange_msg";
        // 4.发送消息
        channel.basicPublish(PSConstant.EXCHANGE_NAME, "", null, msg.getBytes());
        System.out.println("生产者发送msg：" + msg);
        // 5.关闭通道、连接
        channel.close();
        newConnection.close();
        // 注意：如果消费没有绑定交换机和队列，则消息会丢失，所以初始化时要先启动消费者,或者手动创建队列

    }
}
