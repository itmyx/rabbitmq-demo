package com.grape.rabbitmq.queue.core.work;

import com.grape.rabbitmq.queue.util.MQConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 描述:工作队列生产者
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class WorkProducer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.获取连接
        Connection newConnection = MQConnectionUtils.newConnection(WorkConstant.VIRTUALHOST_NAME);
        // 2.创建通道
        Channel channel = newConnection.createChannel();
        // 3.创建队列声明
        channel.queueDeclare(WorkConstant.QUEUE_NAME, false, false, false, null);
        // 保证一次只分发一次 限制发送给同一个消费者 不得超过一条消息
        channel.basicQos(1);
        for (int i = 1; i <= 50; i++) {
            String msg = "test_work" + i;
            System.out.println("生产者发送消息:" + msg);
            // 4.发送消息
            channel.basicPublish("", WorkConstant.QUEUE_NAME, null, msg.getBytes());
        }
        channel.close();
        newConnection.close();
    }
}
