package com.grape.rabbitmq.queue.util;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 描述:rabbitmq连接工具类
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class MQConnectionUtils {

	/**
	 * 创建新的连接
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public static Connection newConnection(String virtualHostName) throws IOException, TimeoutException {
		// 1.定义连接工厂
		ConnectionFactory factory = new ConnectionFactory();
		// 2.设置服务器地址
		factory.setHost("127.0.0.1");
		// 3.设置协议端口号
		factory.setPort(5672);
		// 4.设置vhost
		factory.setVirtualHost(virtualHostName);
		// 5.设置用户名称
		factory.setUsername("myx");
		// 6.设置用户密码
		factory.setPassword("123456");
		// 7.创建新的连接
		Connection newConnection = factory.newConnection();
		return newConnection;
	}

}