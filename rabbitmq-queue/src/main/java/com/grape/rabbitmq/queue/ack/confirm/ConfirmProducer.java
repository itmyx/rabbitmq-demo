package com.grape.rabbitmq.queue.ack.confirm;

import com.grape.rabbitmq.queue.util.MQConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 描述:amqp事务机制生产者
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
public class ConfirmProducer {
    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // 1.获取连接
        Connection newConnection = MQConnectionUtils.newConnection(ConfirmConstant.VIRTUALHOST_NAME);
        // 2.创建通道
        Channel channel = newConnection.createChannel();
        // 3.创建队列声明
        channel.queueDeclare(ConfirmConstant.QUEUE_NAME, false, false, false, null);
        // confirm机制
        channel.confirmSelect();
        String msg = "test_confirm";
        // 4.发送消息
        channel.basicPublish("", ConfirmConstant.QUEUE_NAME, null, msg.getBytes());
        System.out.println("生产者发送消息:" + msg);
        if (!channel.waitForConfirms()) {
            System.out.println("消息发送失败!");
        } else {
            System.out.println("消息发送成功!");
        }
        channel.close();
        newConnection.close();

    }
}
