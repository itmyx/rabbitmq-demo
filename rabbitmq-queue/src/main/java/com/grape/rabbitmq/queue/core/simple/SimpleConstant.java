package com.grape.rabbitmq.queue.core.simple;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class SimpleConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "simple_queue";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/simple";
}
