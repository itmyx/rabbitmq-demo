package com.grape.rabbitmq.queue.core.publishandsubscribe;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class PSConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_EMAIL_NAME = "ps_queue_email";
    public static final String QUEUE_SMS_NAME = "ps_queue_sms";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/ps";
    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "ps_fanout";
}
