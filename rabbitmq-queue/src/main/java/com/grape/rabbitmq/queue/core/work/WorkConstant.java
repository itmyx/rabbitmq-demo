package com.grape.rabbitmq.queue.core.work;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class WorkConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "work_queue";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/work";
}
