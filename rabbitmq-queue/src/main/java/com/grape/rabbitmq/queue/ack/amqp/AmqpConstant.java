package com.grape.rabbitmq.queue.ack.amqp;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
public class AmqpConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "amqp_queue";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/amqp";
}
