package com.grape.rabbitmq.queue.core.simple;

import com.grape.rabbitmq.queue.util.MQConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 描述:简单队列生产者
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class SimpleProducer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.获取连接
        Connection newConnection = MQConnectionUtils.newConnection(SimpleConstant.VIRTUALHOST_NAME);
        // 2.创建通道
        Channel channel = newConnection.createChannel();
        // 3.创建队列声明
        channel.queueDeclare(SimpleConstant.QUEUE_NAME, false, false, false, null);
        String msg = "test_simple";
        System.out.println("生产者发送消息:" + msg);
        // 4.发送消息
        channel.basicPublish("", SimpleConstant.QUEUE_NAME, null, msg.getBytes());
        channel.close();
        newConnection.close();
    }
}
