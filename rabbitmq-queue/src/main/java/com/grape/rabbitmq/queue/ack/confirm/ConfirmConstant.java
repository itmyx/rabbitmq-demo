package com.grape.rabbitmq.queue.ack.confirm;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
public class ConfirmConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "confirm_queue";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/confirm";
}
