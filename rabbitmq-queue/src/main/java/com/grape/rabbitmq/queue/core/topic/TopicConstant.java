package com.grape.rabbitmq.queue.core.topic;

/**
 * 描述:常量信息
 * @author: myx
 * @date: 2019-04-30
 * Copyright © 2019-grape. All rights reserved.
 */
public class TopicConstant {
    /**
     * 队列名称
     */
    public static final String QUEUE_EMAIL_NAME = "topic_queue_email";
    public static final String QUEUE_SMS_NAME = "topic_queue_sms";
    /**
     * virtualHost名称
     */
    public static final String VIRTUALHOST_NAME = "/topic";
    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "topic_exchange";
}
