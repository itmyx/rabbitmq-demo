package com.grape.rebbitmq.springboot.config;

import com.grape.rebbitmq.springboot.core.FanoutConstrant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
/**
 * 描述:交换机绑定队列
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
public class FanoutConfig {

	// 1.定义队列邮件
	@Bean
	public Queue fanOutEamilQueue() {
		return new Queue(FanoutConstrant.QUEUE_EMAIL_NAME);
	}

	@Bean
	public Queue fanOutSmsQueue() {
		return new Queue(FanoutConstrant.QUEUE_SMS_NAME);
	}

	// 2.定义交换机
	@Bean
	FanoutExchange fanoutExchange() {
		return new FanoutExchange(FanoutConstrant.EXCHANGE_NAME);
	}

	// 3.队列与交换机绑定邮件队列
	@Bean
	Binding bindingExchangeEamil(Queue fanOutEamilQueue, FanoutExchange fanoutExchange) {
		return BindingBuilder.bind(fanOutEamilQueue).to(fanoutExchange);
	}

	// 4.队列与交换机绑定短信队列
	@Bean
	Binding bindingExchangeSms(Queue fanOutSmsQueue, FanoutExchange fanoutExchange) {
		return BindingBuilder.bind(fanOutSmsQueue).to(fanoutExchange);
	}
}