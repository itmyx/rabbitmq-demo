package com.grape.rebbitmq.springboot.ack;

import com.grape.rebbitmq.springboot.core.FanoutConstrant;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 描述:签收模式演示
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
public class AckEamilConsumer {
	/**
	 * 消费者
	 * @param message
	 */
	@RabbitListener(queues = FanoutConstrant.QUEUE_EMAIL_NAME)
	public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws IOException {
		System.out.println(Thread.currentThread().getName()
				+ ",邮件消费者获取生产者消息msg:"
				+ new String(message.getBody(), "UTF-8")
				+ ",messageId:" + message.getMessageProperties().getMessageId());
		// 手动ack
		Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
		// 手动签收
		channel.basicAck(deliveryTag, false);
	}
}