package com.grape.rebbitmq.springboot.idempotency;

import com.grape.rebbitmq.springboot.core.FanoutConstrant;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * 描述:幂等性解决邮件消费者
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
public class FanoutIdemEamilConsumer {

	/**
	 * 消费者
	 * @param msg
	 */
	@RabbitListener(queues = FanoutConstrant.QUEUE_EMAIL_NAME)
	public void process(Message msg) throws UnsupportedEncodingException {
		System.out.println(Thread.currentThread().getName()
				+ ",邮件消费者获取生产者消息msg:"
				+ new String(msg.getBody(), "UTF-8")
				+ ",messageId:" + msg.getMessageProperties().getMessageId());
		//如果处理过程中出现错误，默认为一直消费数据(可以通过application.yml配置修改重试次数)
		//int i=1/0;
	}
}