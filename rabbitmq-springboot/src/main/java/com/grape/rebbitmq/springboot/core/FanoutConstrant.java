package com.grape.rebbitmq.springboot.core;

/**
 * 描述:常量
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
public class FanoutConstrant {
    /**
     * 队列名称
     */
    public static final String QUEUE_EMAIL_NAME = "fanout_queue_email";
    public static final String QUEUE_SMS_NAME = "fanout_queue_sms";
    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "fanout";
}
