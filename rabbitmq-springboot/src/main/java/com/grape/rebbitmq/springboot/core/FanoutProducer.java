package com.grape.rebbitmq.springboot.core;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
/**
 * 描述:生产者
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
public class FanoutProducer {
	@Autowired
	private AmqpTemplate amqpTemplate;

	/**
	 * 发送消息
	 * @param queueName 队列名称
	 */
	public void send(String queueName) {
		String msg = "my_fanout_msg:" + new Date();
		System.out.println(msg + ":" + msg);
		amqpTemplate.convertAndSend(queueName, msg);
	}
}