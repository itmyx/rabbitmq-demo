package com.grape.rebbitmq.springboot.core;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
/**
 * 描述:邮件消费者
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
@RabbitListener(queues = FanoutConstrant.QUEUE_EMAIL_NAME)
public class FanoutEamilConsumer {
	@RabbitHandler
	public void process(String msg){
		System.out.println("邮件消费者获取生产者消息msg:" + msg);
		//如果处理过程中出现错误，默认为一直消费数据(可以通过application.yml配置修改重试次数)
		//int i=1/0;
	}
}