package com.grape.rebbitmq.springboot.idempotency;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

/**
 * 描述:幂等性解决生产者
 * @author: myx
 * @date: 2019-05-01
 * Copyright © 2019-grape. All rights reserved.
 */
@Component
public class FanoutIdemProducer {
	@Autowired
	private AmqpTemplate amqpTemplate;

	/**
     * 发送消息
	 * @param queueName 队列名称
	 */
	public void send(String queueName) {
		String msg = "my_fanout_msg:" + System.currentTimeMillis();
		//设置消息头
		Message message = MessageBuilder.withBody(msg.getBytes()).setContentType(MessageProperties.CONTENT_TYPE_JSON)
				.setContentEncoding("utf-8").setMessageId(UUID.randomUUID() + "").build();
		System.out.println(msg + ":" + msg);
		amqpTemplate.convertAndSend(queueName, message);
	}
}